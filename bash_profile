echo "Input password to setup xorg"
sudo X -configure
sudo cp /root/xorg.conf.new /etc/X11/xorg.conf
cp ~/.bash_profile ~/.old_profile
#This copies ranger config
ranger --copy-config=all
cp ~/rc.conf ~/.config/ranger/rc.conf
rm rc.conf
echo startx > .bash_profile
startx
